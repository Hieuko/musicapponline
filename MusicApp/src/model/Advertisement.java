package model;

public class Advertisement {
	private long id;
	private String image;
	private String content;
	private Song song;
	
	public Advertisement(long id, String image, String content, Song song) {
		super();
		this.id = id;
		this.image = image;
		this.content = content;
		this.song = song;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}
	
	
	
}
