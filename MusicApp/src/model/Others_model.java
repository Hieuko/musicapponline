package model;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Others_model {
	public static boolean checkString(String str) {
		for(int i = 0 ; i < str.length(); i++) {
			if(str.charAt(i) > 57 || str.charAt(i) < 48) {
				return false;
			} 
		}
		return true;
	}
	
	public static long numberOfPage(long amount, long amountPerPage ) {
		long result = amount/amountPerPage;
		if(amount % amountPerPage != 0) {
			result = result + 1;
		}
		return result;
	}
	
	public static String md5(String str){
		String result = "";
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.update(str.getBytes());
			BigInteger bigInteger = new BigInteger(1,digest.digest());
			result = bigInteger.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return result;
	}
}
