package model;

public class Song {
	private long id;
	private String name;
	private String image;
	private String singer;
	private String url;
	private String album_id;
	private String category_id;
	private String playlist_id;
	private int like_number;
	
	public Song(long id, String name, String image, String singer, String url, String album_id, String category_id,
			String playlist_id, int like_number) {
		super();
		this.id = id;
		this.name = name;
		this.image = image;
		this.singer = singer;
		this.url = url;
		this.album_id = album_id;
		this.category_id = category_id;
		this.playlist_id = playlist_id;
		this.like_number = like_number;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAlbum_id() {
		return album_id;
	}

	public void setAlbum_id(String album_id) {
		this.album_id = album_id;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getPlaylist_id() {
		return playlist_id;
	}

	public void setPlaylist_id(String playlist_id) {
		this.playlist_id = playlist_id;
	}

	public int getLike_number() {
		return like_number;
	}

	public void setLike_number(int like_number) {
		this.like_number = like_number;
	}
	
	
	
}
