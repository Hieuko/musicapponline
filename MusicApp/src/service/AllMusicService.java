package service;

import java.sql.SQLException;
import java.util.ArrayList;

import dao.AdvertisementDAO;
import dao.AlbumDAO;
import dao.CategoryDAO;
import dao.PlaylistDAO;
import dao.SongDAO;
import dao.TopicDAO;
import model.Advertisement;
import model.Album;
import model.Category;
import model.Others_model;
import model.Playlist;
import model.Song;
import model.Topic;

public class AllMusicService {
	public ArrayList<Advertisement> getAllAdvertisement() throws ClassNotFoundException, SQLException{
		return AdvertisementDAO.getAllAdvertisement();
	}
	
	public ArrayList<Playlist> getRandomPlaylists(String status) throws ClassNotFoundException, SQLException{
		if(status.equals("true")) {
			return PlaylistDAO.getRandomPlaylists();
		} else {
			return PlaylistDAO.getAllPlaylist();
		}
	}
	
	public ArrayList<Topic> getRandomTopic(String status) throws ClassNotFoundException, SQLException {
		if(status.equals("true")) {
			return TopicDAO.getRandomTopic();
		} else {
			return TopicDAO.getAllTopic();
		}
	}
	
	
	public ArrayList<Category> getRandomCategory(String status) throws ClassNotFoundException, SQLException {
		if(status.equals("true")) {
			return CategoryDAO.getRandomCategory();
		} else {
			return CategoryDAO.getAllCategory();
		}
	}
	
	public ArrayList<Category> getCategoryFromTopic(String id) throws ClassNotFoundException, SQLException {
		if(Others_model.checkString(id) == true) {
			long topic_id = Long.parseLong(id);
			return CategoryDAO.getCategoryFromTopic(topic_id);
		} else {
			return null;
		}
	}
	
	public ArrayList<Album> getRandomAlbum(String status) throws ClassNotFoundException, SQLException {
		if(status.equals("true")) {
			return AlbumDAO.getRandomAlbum();
		} else {
			return AlbumDAO.getAllAlbum();
		}
	}
	
	public ArrayList<Song> getPopularSong() throws ClassNotFoundException, SQLException{
		return SongDAO.getPopularSong();
	}
	
	public ArrayList<Song> getSongInPlaylist(String id) throws ClassNotFoundException, SQLException{
		return SongDAO.getSongInPlaylist(id);
	}
	
	public ArrayList<Song> getSongInAlbum(String id) throws ClassNotFoundException, SQLException{
		return SongDAO.getSongInAlbum(id);
	}
	
	public ArrayList<Song> getSongInCategory(String id) throws ClassNotFoundException, SQLException{
		return SongDAO.getSongInCategory(id);
	}
	
	public ArrayList<Song> getFindSong(String id) throws ClassNotFoundException, SQLException{
		return SongDAO.getFindSong(id);
	}
	
	public String incLikeNumber(String id) throws ClassNotFoundException, SQLException {
		int a = 0;
		if(Others_model.checkString(id) == true) {
			long song_id = Long.parseLong(id);
			a = SongDAO.incLikeNumber(song_id);
		}		
		if ( a == 0) {
			return "fail";
		} else {
			return "success";
		}
	}
	
	
}
