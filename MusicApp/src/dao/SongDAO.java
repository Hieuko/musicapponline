package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Song;

public class SongDAO {
	public static ArrayList<Song> getPopularSong() throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		
		CallableStatement cs = conn.prepareCall("{call getTop5LikeSong}");
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		
		ArrayList<Song> list = new ArrayList<Song>();
		while(rs.next()) {
			Song song = new Song(rs.getLong("song_id"),rs.getString("song_name"),rs.getString("song_image")
					,rs.getString("song_singer"),rs.getString("song_url"),rs.getString("album_id")
					,rs.getString("category_id"),rs.getString("playlist_id"),rs.getInt("like_number"));
			list.add(song);
		}
		conn.close();
		return list;
	}
	
	public static ArrayList<Song> getSongInPlaylist(String plalist_id) throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getSongInPlaylist(?)}");
		cs.setString("playlist_id", plalist_id);
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		
		ArrayList<Song> list = new ArrayList<Song>();
		while(rs.next()) {
			Song song = new Song(rs.getLong("song_id"),rs.getString("song_name"),rs.getString("song_image")
					,rs.getString("song_singer"),rs.getString("song_url"),rs.getString("album_id")
					,rs.getString("category_id"),rs.getString("playlist_id"),rs.getInt("like_number"));
			list.add(song);
		}
		conn.close();
		return list;
	}
	
	public static ArrayList<Song> getSongInAlbum(String album_id) throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getSongInAlbum(?)}");
		cs.setString("album_id", album_id);
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		ArrayList<Song> list = new ArrayList<Song>();
		while(rs.next()) {
			Song song = new Song(rs.getLong("song_id"),rs.getString("song_name"),rs.getString("song_image")
					,rs.getString("song_singer"),rs.getString("song_url"),rs.getString("album_id")
					,rs.getString("category_id"),rs.getString("playlist_id"),rs.getInt("like_number"));
			list.add(song);
		}
		conn.close();
		return list;
	}
	
	public static ArrayList<Song> getSongInCategory(String category_id) throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getSongInCategory(?)}");
		cs.setString("category_id", category_id);
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		
		ArrayList<Song> list = new ArrayList<Song>();
		while(rs.next()) {
			Song song = new Song(rs.getLong("song_id"),rs.getString("song_name"),rs.getString("song_image")
					,rs.getString("song_singer"),rs.getString("song_url"),rs.getString("album_id")
					,rs.getString("category_id"),rs.getString("playlist_id"),rs.getInt("like_number"));
			list.add(song);
		}
		conn.close();
		return list;
	}
	
	public static int incLikeNumber(long song_id) throws ClassNotFoundException, SQLException {
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call incLikeNumber(?)}");
		cs.setLong("song_id", song_id);
		int a = cs.executeUpdate();
		conn.close();
		return a;
	}
	
	public static ArrayList<Song> getFindSong(String key) throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call findSong(?)}");
		cs.setString("key", key.toLowerCase());
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		
		ArrayList<Song> list = new ArrayList<Song>();
		while(rs.next()) {
			Song song = new Song(rs.getLong("song_id"),rs.getString("song_name"),rs.getString("song_image")
					,rs.getString("song_singer"),rs.getString("song_url"),rs.getString("album_id")
					,rs.getString("category_id"),rs.getString("playlist_id"),rs.getInt("like_number"));
			list.add(song);
		}
		conn.close();
		return list;
	}
	
}
