package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Topic;

public class TopicDAO {
	public static ArrayList<Topic> getRandomTopic() throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		
		CallableStatement cs = conn.prepareCall("{call getRandom4Topic}");
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		ArrayList<Topic> list = new ArrayList<Topic>();
		
		while(rs.next()) {
			Topic topic = new Topic();
			topic.setId(rs.getLong("topic_id"));
			topic.setName(rs.getString("topic_name"));
			topic.setImage(rs.getString("topic_image"));
			
			list.add(topic);
		}
		conn.close();
		return list;	
	}
	
	public static ArrayList<Topic> getAllTopic() throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		
		CallableStatement cs = conn.prepareCall("{call getAllTopic}");
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		ArrayList<Topic> list = new ArrayList<Topic>();
		
		while(rs.next()) {
			Topic topic = new Topic();
			topic.setId(rs.getLong("topic_id"));
			topic.setName(rs.getString("topic_name"));
			topic.setImage(rs.getString("topic_image"));
			
			list.add(topic);
		}
		conn.close();
		return list;	
	}
}
