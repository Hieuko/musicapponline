package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Advertisement;
import model.Song;

public class AdvertisementDAO {
	public static ArrayList<Advertisement> getAllAdvertisement() throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call all_advertisement}");
		ResultSet rs = SQLDatabase.getResultsSet(cs);
	
		ArrayList<Advertisement> list = new ArrayList<Advertisement>();
		
		while(rs.next()) {
			Song song = new Song(rs.getLong("song_id"),rs.getString("song_name"),rs.getString("song_image")
					,rs.getString("song_singer"),rs.getString("song_url"),rs.getString("album_id")
					,rs.getString("category_id"),rs.getString("playlist_id"),rs.getInt("like_number"));
			
			Advertisement adv = new Advertisement(rs.getLong("advertisement_id"), rs.getString("advertisement_image")
					, rs.getString("advertisement_content"), song);
			
			list.add(adv);
		}
		conn.close();
		return list;	
	}
}
