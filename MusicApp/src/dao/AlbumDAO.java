package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Album;

public class AlbumDAO {
	public static ArrayList<Album> getRandomAlbum() throws ClassNotFoundException, SQLException {
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getRandom4Album}");
		
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		ArrayList<Album> list = new ArrayList<Album>();
		
		while(rs.next()) {
			Album album = new Album();
			album.setId(rs.getLong("album_id"));
			album.setImage(rs.getString("album_image"));
			album.setName(rs.getString("album_name"));
			album.setSinger(rs.getString("album_singer"));
			
			list.add(album);
		}
		conn.close();
		return list;
	}
	
	public static ArrayList<Album> getAllAlbum() throws ClassNotFoundException, SQLException {
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getAllAlbum}");
		
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		ArrayList<Album> list = new ArrayList<Album>();
		
		while(rs.next()) {
			Album album = new Album();
			album.setId(rs.getLong("album_id"));
			album.setImage(rs.getString("album_image"));
			album.setName(rs.getString("album_name"));
			album.setSinger(rs.getString("album_singer"));
			
			list.add(album);
		}
		conn.close();
		return list;
	}
}
