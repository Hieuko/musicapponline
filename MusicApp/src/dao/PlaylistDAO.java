package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Playlist;

public class PlaylistDAO {
	public static ArrayList<Playlist> getRandomPlaylists() throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		
		CallableStatement cs = conn.prepareCall("{call getRandom3Playlist}");
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		ArrayList<Playlist> list = new ArrayList<Playlist>();
		
		while(rs.next()) {
			Playlist playlist = new Playlist();
			playlist.setId(rs.getLong("playlist_id"));
			playlist.setName(rs.getString("playlist_name"));
			playlist.setBackground(rs.getString("playlist_background"));
			playlist.setIcon(rs.getString("playlist_icon"));
			
			list.add(playlist);
		}
		conn.close();
		return list;	
	}
	
	public static ArrayList<Playlist> getAllPlaylist() throws ClassNotFoundException, SQLException{
		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getAllPlaylist}");
		ResultSet rs = SQLDatabase.getResultsSet(cs);
		
		ArrayList<Playlist> list = new ArrayList<Playlist>();
		while(rs.next()) {
			Playlist playlist = new Playlist();
			playlist.setId(rs.getLong("playlist_id"));
			playlist.setName(rs.getString("playlist_name"));
			playlist.setBackground(rs.getString("playlist_background"));
			playlist.setIcon(rs.getString("playlist_icon"));
			
			list.add(playlist);
		}
		conn.close();
		return list;
	}
}
