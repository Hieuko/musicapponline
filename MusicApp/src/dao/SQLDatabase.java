package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLDatabase {
	//lấy chuỗi kết nối
	public static Connection connectionString() throws SQLException, ClassNotFoundException {
		String dbURL = "jdbc:sqlserver://localhost:1433;"
				+"databaseName=MusicApp";
//				+"intergratedSeurity=true";
		String USER_NAME = "sa";
		String PASSWORD = "123456";
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection conn = DriverManager.getConnection(dbURL, USER_NAME, PASSWORD);
		return conn;
	}
	
	//trả về resultSet
	public static ResultSet getResultsSet(CallableStatement cs) throws SQLException {
		ResultSet rs = null;
		cs.execute();
		rs = cs.getResultSet();
		return rs;
	}
	
}
