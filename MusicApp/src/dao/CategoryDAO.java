package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Category;

public class CategoryDAO {
	public static ArrayList<Category> getRandomCategory() throws ClassNotFoundException, SQLException{

		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getRandom4Category}");

		ResultSet rs = SQLDatabase.getResultsSet(cs);

		ArrayList<Category> list = new ArrayList<Category>();
		while(rs.next()) {
			Category category = new Category();

			category.setId(rs.getLong("category_id"));
			category.setTopic_id(rs.getLong("topic_id"));
			category.setImage(rs.getString("category_image"));
			category.setName(rs.getString("category_name"));

			list.add(category);
		}
		conn.close();
		return list;

	}

	public static ArrayList<Category> getAllCategory() throws ClassNotFoundException, SQLException{

		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getAllCategory}");

		ResultSet rs = SQLDatabase.getResultsSet(cs);
		conn.close();
		ArrayList<Category> list = new ArrayList<Category>();
		while(rs.next()) {
			Category category = new Category();

			category.setId(rs.getLong("category_id"));
			category.setTopic_id(rs.getLong("topic_id"));
			category.setImage(rs.getString("category_image"));
			category.setName(rs.getString("category_name"));

			list.add(category);
		}
		conn.close();
		return list;

	}
	
	public static ArrayList<Category> getCategoryFromTopic(long topic_id) throws ClassNotFoundException, SQLException{

		Connection conn = SQLDatabase.connectionString();
		CallableStatement cs = conn.prepareCall("{call getCategoryFromTopic(?)}");
		cs.setLong("topic_id", topic_id);

		ResultSet rs = SQLDatabase.getResultsSet(cs);

		ArrayList<Category> list = new ArrayList<Category>();
		while(rs.next()) {
			Category category = new Category();

			category.setId(rs.getLong("category_id"));
			category.setTopic_id(rs.getLong("topic_id"));
			category.setImage(rs.getString("category_image"));
			category.setName(rs.getString("category_name"));

			list.add(category);
		}
		conn.close();
		return list;

	}
}
